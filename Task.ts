import * as mongoose from 'mongoose';
import { mongoConn } from './Bot';
import { ITaskbook } from './Taskbook';
import { IUser } from './User';
const Schema = mongoose.Schema;

export enum TaskStatus {
    NotReady,
    Ready
}

export interface ITask extends mongoose.Document {
    user: mongoose.Types.ObjectId | IUser;
    name: string;
    description: string;
    status: TaskStatus;
    deadline: number;
    daysOfCompletion: number[];
    taskbook: mongoose.Types.ObjectId | ITaskbook;
    parent: mongoose.Types.ObjectId | ITask;
    target: number;
    current: number;

    //getProgressStr(): string;
}

export const taskSchema = new Schema({
    user: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'User'
    },
    name: String,
    description: String,
    status: {
        type: Number,
        enum: TaskStatus,
        default: TaskStatus.NotReady
    },
    deadline: Number,
    daysOfCompletion: Array,
    taskbook: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Taskbook'
    },
    parent: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Task'
    },
    target: Number,
    current: Number
});
taskSchema.virtual('subtasks', {
    ref: 'Task', // The model to use
    localField: 'parent', // Find people where `localField`
    foreignField: '_id', // is equal to `foreignField`
    // If `justOne` is true, 'members' will be a single doc as opposed to
    // an array. `justOne` is false by default.
    justOne: false
  });
// taskScheme.methods.getProgressStr = function(this: ITask) : string {
//     if(!this.subtasks.length) {
//         return `${this.current/this.target*100} (${this.current}/${this.target})`;
//     }
//     else 
//     {
//         const complitedTasksAmount = this.subtasks.filter((task) => task.status == TaskStatus.Ready).length;
//         return `${complitedTasksAmount/this.subtasks.length*100} (${complitedTasksAmount}/${this.subtasks.length})`;
//     }
// }

export const Task = mongoConn.model<ITask>("Task", taskSchema);