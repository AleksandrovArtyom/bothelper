import * as mongoose from 'mongoose';
import { mongoConn } from './Bot';
import { IUser } from './User';
const Schema = mongoose.Schema;

export enum TaskbookUserRole {
    User,
    Moderator,
    Administrator
}

export interface ITaskbookUser extends mongoose.Document {
    user: [mongoose.Types.ObjectId | IUser];
    role: TaskbookUserRole;
}
export const taskbookUserSchema = new Schema({
    user: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'User'
    },
    role: {
        type: Number,
        enum: TaskbookUserRole,
        default: TaskbookUserRole.User
    }
})


export interface ITaskbook extends mongoose.Document {
    users: [mongoose.Types.ObjectId | ITaskbookUser];
    name: string;
    
    // tasks: Task[]   
}


export const taskbookSchema = new Schema({
    users: [taskbookUserSchema],
    name: String
});

export const Taskbook = mongoConn.model<ITaskbook>("Taskbook", taskbookSchema);