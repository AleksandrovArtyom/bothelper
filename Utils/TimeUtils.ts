
export function getDaysGlobally() {
    const now = new Date();
    const ms = now.getTime();
    const days = ms / 1000 / 60 / 60 / 24;
    return days;
}