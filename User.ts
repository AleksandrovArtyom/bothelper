import * as mongoose from 'mongoose';
import { mongoConn } from './Bot';
const Schema = mongoose.Schema;

export interface IUser extends mongoose.Document {
    messangerUserId: number;
    username: string;
    // tasks: Task[]   
}


export const userScheme = new Schema({
    messangerUserId: Number,
    username: String
});

export const User = mongoConn.model<IUser>("User", userScheme);