import { InlineButtonClickedEvent } from "abstractbot/Bot/BotEvent";
import { Keyboard } from "abstractbot/Types/Keyboard/Keyboard";
import { KeyboardButton } from "abstractbot/Types/Keyboard/KeyboardButton";
import { MessageContent } from "abstractbot/Types/Message/MessageContent";
import { bot, tgMessanger } from "../Bot";
import { ITask, Task, TaskStatus } from "../Task";
import { IUser, User } from "../User";
import { getDaysGlobally } from "../Utils/TimeUtils";

import * as mongoose from 'mongoose'
import { MessangerChat } from "abstractbot/Types/MessangerChat";


export async function sendTask(chat: MessangerChat, task: ITask) {
    const msg = new MessageContent(
        `<b>${task.name}</b>
${task.description}
${task.deadline ? `Дедлайн: ${task.deadline}` : ``}`,
        undefined,
        await generateTaskMenu(task)
    )

    bot.sendMessage(chat, msg, `task`);
}

export async function sendAllTasks(chat: MessangerChat, user: IUser) {
    const curDay = getDaysGlobally();

    const tasks = await Task.find({user: user._id}).exec();

    //let thisDayTasks = user.tasks.filter(task => {task.daysOfCompletion.some(day => day == curDay)});

    tasks.forEach(task => sendTask(chat, task));
}






// === Default Keyboard
const finishTaskButton = new KeyboardButton(`✔️ Завершить`, `finishTask`);
const returnTaskButton = new KeyboardButton(`❌ Задача завершена`, `finishTask`);
const addToTodayButton = new KeyboardButton(`Добавить задачу в список на сегодня`, `addToToday`);
const setReminderButton = new KeyboardButton(`Установить напоминание`, '')


// === Today Keyboard

const completeTaskButton = new KeyboardButton(`✔️ Выполнено`, `markTask`);
const uncompleteTaskButton = new KeyboardButton(`❌ Не выполнено `, `markTask`);
const editTaskButton = new KeyboardButton(`📗 Редактировать`, `editTask`);
const subtasksButton = new KeyboardButton(`📗 Подзадачи`, `subtasks`);
const setProgressButton = new KeyboardButton(`📗 Установить прогресс`, `setProgress`);
const addSubtaskButton = new KeyboardButton(`📗 Добавить подзадачу`, `addSubtask`);
async function generateTaskMenu(task: ITask) {
    await task.populate('subtasks').execPopulate();
    const subtasks: ITask[] = task.populated('subtasks');
    //const test = await Task.findOne({_id:""}).populate('subtasks').exec();

    const inlineMenu = new Keyboard([
        [
            (task.status == TaskStatus.Ready) ? completeTaskButton : uncompleteTaskButton 
        ],
        [
            editTaskButton
        ],
        (subtasks.length > 0) ? 
            [
                subtasksButton
            ]
                : 
            [
                setProgressButton,
                addSubtaskButton
            ]
    ], true)
    return inlineMenu;
};