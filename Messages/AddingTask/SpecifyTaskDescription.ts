import { NewMessageEvent } from "abstractbot/Bot/BotEvent";
import { MessageContent } from "abstractbot/Types/Message/MessageContent";
import { MessangerChat } from "abstractbot/Types/MessangerChat";
import { bot, tgMessanger } from "../../Bot";
import { ITask, Task } from "../../Task";


export function specifyTaskDescription(chat: MessangerChat, task: ITask) {
    const msg = new MessageContent(
        `🤖 Напиши описание задачи:`,
    );

    bot.sendMessage(chat, msg, `specifyTaskDescription`, {task});
}

bot.handlerScheme.addHandler(`specifyTaskDescription`, {
    handleEvent: (event: NewMessageEvent, data) => {
        if(!event.message.text) return;

        const task: ITask = data.task;
        task.description = event.message.text;
        specifyTaskDescription(event.chat, task);
    }
})
