import { NewMessageEvent } from "abstractbot/Bot/BotEvent";
import { MessageContent } from "abstractbot/Types/Message/MessageContent";
import { MessangerChat } from "abstractbot/Types/MessangerChat";
import { bot, tgMessanger } from "../../Bot";
import { ITask, Task } from "../../Task";
import { specifyTaskDescription } from "./SpecifyTaskDescription";


export function specifyTaskName(chat: MessangerChat, task: ITask) {
    const msg = new MessageContent(
        `🤖 Напиши название задачи:`,
    );

    bot.sendMessage(chat, msg, `specifyTaskName`, {task});
}

bot.handlerScheme.addHandler(`specifyTaskName`, {
    handleEvent: (event: NewMessageEvent, data) => {
        if(!event.message.text) return;

        const task: ITask = data.task;
        task.name = event.message.text;
        specifyTaskDescription(event.chat, task);
    }
})
