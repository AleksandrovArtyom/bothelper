import { InlineButtonClickedEvent } from "abstractbot/Bot/BotEvent";
import { Keyboard } from "abstractbot/Types/Keyboard/Keyboard";
import { KeyboardButton } from "abstractbot/Types/Keyboard/KeyboardButton";
import { MessageContent } from "abstractbot/Types/Message/MessageContent";
import { MessangerChat } from "abstractbot/Types/MessangerChat";
import { bot, tgMessanger } from "../../Bot";
import { User } from "../../User";
import { sendAllTasks } from "../TaskMessage";

export function sendTaskbookMenuMessage(chat: MessangerChat) {
    const msg = new MessageContent(
        `Меню задачника:`,
        undefined, 
        taskManagerMenu,
    )
    bot.sendMessage(chat, msg, 'taskbookMenu');
}

bot.handlerScheme.addHandler(`taskbookMenu`, {
    handleEvent: async (event: InlineButtonClickedEvent, data) => {
        if (!event.data) return;

        const user = await User.findOne({_id: event.chat.id}).exec();

        const buttonName = event.data;
        switch (buttonName) {
            case 'allTasks': {
                sendAllTasks(event.chat, user!);
                break;
            }
        }
    }
})

const todayTasksButton = new KeyboardButton(`📗 Задачи на сегодня`, `todayTasks`);
const allTasksButton = new KeyboardButton(`📗 Общий список задач`, `alltasks`);
const taskManagerMenu = new Keyboard(
    [
        [
            todayTasksButton,
            allTasksButton
        ]
    ], true
);
