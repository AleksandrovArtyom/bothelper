import { InlineButtonClickedEvent } from "abstractbot/Bot/BotEvent";
import { Keyboard } from "abstractbot/Types/Keyboard/Keyboard";
import { KeyboardButton } from "abstractbot/Types/Keyboard/KeyboardButton";
import { MessageContent } from "abstractbot/Types/Message/MessageContent";
import { MessangerChat } from "abstractbot/Types/MessangerChat";
import { bot, tgMessanger } from "../../Bot";
import { Taskbook } from "../../Taskbook";
import { IUser, User } from "../../User";
import { startNewTaskbook } from "./Creating/NewTaskbook";

export async function sendTaskbooksListMessage(chat: MessangerChat) {
    const user = await User.findOne({messangerUserId: <number>chat.id}).exec();
    if(!user) return;

    const taskbookKeyboard = await getTaskbooksKeyboard(user);

    const msg = new MessageContent(
        `Задачники:`,
        undefined, 
        taskbookKeyboard,
    )
    bot.sendMessage(chat, msg, 'taskbooksList');
}

bot.handlerScheme.addHandler(`taskbooksList`, {
    handleEvent: async (event: InlineButtonClickedEvent, data) => {
        if (!event.data) return;

        const buttonName = event.data;
        switch (buttonName) {
            case 'newTaskbook': {
                startNewTaskbook(event.chat);
                break;
            }
            default: {
                const taskbookId = event.data;
                const taskbook = await Taskbook.find({_id: taskbookId}).exec();

                break;
            }
        }
    }
})

const newTaskbookButton = new KeyboardButton(`➕ Новый задачник`, `newTaskbook`);
async function getTaskbooksKeyboard(user: IUser) {
    const keyboardButtons: KeyboardButton[][] = [
        [newTaskbookButton]
    ];

    const taskbooks = await Taskbook.find({user: user._id}).exec();
    for(let i = 0; i < taskbooks.length; i += 2) {
        keyboardButtons.push([
            new KeyboardButton(taskbooks[i].name, taskbooks[i]._id),
            new KeyboardButton(taskbooks[i+1].name, taskbooks[i+1]._id),
        ])
    }

    return new Keyboard(keyboardButtons, false);
}

