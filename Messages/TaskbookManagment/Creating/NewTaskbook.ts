import { NewMessageEvent } from "abstractbot/Bot/BotEvent";
import { Chat } from "abstractbot/Types/Chat";
import { MessageContent } from "abstractbot/Types/Message/MessageContent";
import { bot, tgMessanger } from "../../../Bot";
import { specifyTaskbookUsers } from "./SpecifyTaskbookUsers";

export async function startNewTaskbook(chat: Chat) {
    const msg = new MessageContent(
        `Создание нового задачника.
Укажи название задачника.`
    )
    bot.sendMessage(tgMessanger, chat, msg, 'specifyingTaskbookName');
}

bot.handlerScheme.addHandler(`specifyingTaskbookName`, {
    handleEvent: async (event: NewMessageEvent, data) => {
        //TODO: Add error messages
        if (!event.message.text) return;
        if (event.message.text.length > 64) return;
        console.log(`Checks passed`)

        const taskbookName = event.message.text;
        const taskbookData = {
            taskbookName: taskbookName
        }

        specifyTaskbookUsers(event.chat, taskbookData);
    }
})
