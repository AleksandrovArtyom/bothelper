import { InlineButtonClickedEvent, NewMessageEvent } from "abstractbot/Bot/BotEvent";
import { Chat } from "abstractbot/Types/Chat";
import { Keyboard } from "abstractbot/Types/Keyboard/Keyboard";
import { KeyboardButton } from "abstractbot/Types/Keyboard/KeyboardButton";
import { MessageContent } from "abstractbot/Types/Message/MessageContent";
import { bot, tgMessanger } from "../../../Bot";
import { Taskbook, TaskbookUserRole } from "../../../Taskbook";
import { IUser, User } from "../../../User";

export async function specifyTaskbookUsers(chat: Chat, taskbookData: any) {
    const msg = new MessageContent(
        `Перечислите пользователей задачника через запятую.
Например: "@username1, @username2".`,
        undefined,
        keyboard
    )
    bot.sendMessage(tgMessanger, chat, msg, 'specifyingTaskbookUsers', taskbookData);
}

bot.handlerScheme.addHandler(`specifyingTaskbookUsers`, {
    handleEvent: async (event: NewMessageEvent, msgCache) => {

        console.log(msgCache);

        //TODO: Add error messages
        if (!event.message.text) return;
        
        const str = event.message.text;
        const usernameRegExp = /@([a-z0-9])+\b/g
        const username = str.match(usernameRegExp);
        const usernames: string[] = username!.map((value) => value);

        const user = await User.findOne({messangerUserId: event.userId}).exec();
        const taskbook = await Taskbook.create({
            users: [{
                user: user?._id,
                role: TaskbookUserRole.Administrator
            }],
            name: msgCache.data.taskbookName
        })

        const msg = new MessageContent(
            `Успешно создан задачник ${msgCache.data.taskbookName}`
        )
        bot.sendMessage(tgMessanger, event.chat, msg);
    }
})

const skipButton = new KeyboardButton(`Пропустить`, `skip`);
const keyboard = new Keyboard(
    [
        [
            skipButton
        ]
    ], false
);
