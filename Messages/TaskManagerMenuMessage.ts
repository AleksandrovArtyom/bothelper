import { InlineButtonClickedEvent } from "abstractbot/Bot/BotEvent";
import { Keyboard } from "abstractbot/Types/Keyboard/Keyboard";
import { KeyboardButton } from "abstractbot/Types/Keyboard/KeyboardButton";
import { MessageContent } from "abstractbot/Types/Message/MessageContent";
import { MessangerChat } from "abstractbot/Types/MessangerChat";
import { bot, tgMessanger } from "../Bot";
import { sendTaskbooksListMessage } from "./TaskbookManagment/TaskbooksListMessage";

export function sendTaskManagerMessage(chat: MessangerChat) {
    const msg = new MessageContent(
        `Меню задач:`,
        undefined, 
        taskManagerMenu,
    )
    bot.sendMessage(chat, msg, 'taskManagerMenu');
}

bot.handlerScheme.addHandler(`taskManagerMenu`, {
    handleEvent: (event: InlineButtonClickedEvent, data) => {
        if (!event.data) return;

        const buttonName = event.data;
        switch (buttonName) {
            // case 'allTodayTasks': {
            //     sendTodayTasks(event.chat);
            //     break;
            // }
            case 'taskBooks': {
                sendTaskbooksListMessage(event.chat);
                break;
            }
            // case 'settings': {
            //     sendAccountSettings(event.chat);
            //     break;
            // }
        }
    }
})

const todayTasksButton = new KeyboardButton(`📓 Сегодняшние задачи`, `allTodayTasks`);
const taskBooksButton = new KeyboardButton(`📚 Задачники`, `taskBooks`);
const settingsButton = new KeyboardButton(`⚙️ Личные настройки`, `settings`);
const taskManagerMenu = new Keyboard(
    [
        [
            todayTasksButton
        ],
        [
            taskBooksButton
        ],
        [
            settingsButton
        ]
    ], true
);
