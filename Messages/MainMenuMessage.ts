import { InlineButtonClickedEvent } from "abstractbot/Bot/BotEvent";
import { Keyboard } from "abstractbot/Types/Keyboard/Keyboard";
import { KeyboardButton } from "abstractbot/Types/Keyboard/KeyboardButton";
import { MessageContent } from "abstractbot/Types/Message/MessageContent";
import { MessangerChat } from "abstractbot/Types/MessangerChat";
import { button } from "telegraf/typings/markup";
import { bot, tgMessanger } from "../Bot";
import { sendTaskManagerMessage } from "./TaskManagerMenuMessage";


export function sendMainMenu(chat: MessangerChat) {
    const msg = new MessageContent(
        `🤖 Привет! Бот Хелпер к твоим услугам!`,
        undefined,
        mainInlineMenu
    );

    bot.sendMessage(chat, msg, `mainMenu`);
}

bot.handlerScheme.addHandler(`mainMenu`, {
    handleEvent: (event: InlineButtonClickedEvent, data) => {
        console.log(`triggered`);
        if (!event.data) return;

        const buttonName = event.data;
        switch (buttonName) {
            case 'tasks': {
                sendTaskManagerMessage(event.chat);
                break;
            }
        }
    }
})

const tasksButtonMainMenuButton = new KeyboardButton(`📗 Задачи`, `tasks`);
const mainInlineMenu = new Keyboard(
    [
        [
            tasksButtonMainMenuButton
        ]
    ], false
);